# RM_Trainning_Tasks

**本仓库仅供中山大学逸仙狮机器人队用于新队员的培训，欢迎学习参考，转载或用于团队建设需提前告知。**

## [机械组培训任务](https://gitlab.com/sysulion_code/TrainningTasks)
机械组成员主要在SolidWorks软件平台进行三维建图设计，所以需要掌握与熟练运用Sw的基本功能，学习各种常见零件的建图、结构装配等。学习资料链接请戳[这里](https://gitlab.com/sysulion_code/TrainningTasks)。
## [电控组培训任务](https://gitlab.com/sysulion_code/standard)
电控组成员主要在Keil上进行单片机的开发与学习，并使用gitlab进行代码多人协作管理，主要开发语言为C语言。学习资料链接请戳[这里](https://gitlab.com/sysulion_code/standard)。
## [视觉组任务](https://gitlab.com/sysulion_code/AlgorithmTasks)
视觉组成员需掌握一定图像处理、定位与建图算法等知识，故门槛比较高，欢迎各位高年级的同学挑战！具体任务请戳[这里](https://gitlab.com/sysulion_code/AlgorithmTasks)。
    

